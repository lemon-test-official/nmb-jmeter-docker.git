# 使用说明

## 一台机器上构建多个jmeter助攻机容器

1、下载【slave-jmeter-docker】文件夹中所有文件

2、构建本地slave镜像： `sh build.sh`

3、创建slave容器

```sh
docker run -itd --name slave1 nmb/jmeter-slave:5.1.1 server

# 重复执行时，修改容器名称name值，则可创建多个slave容器
```

> 默认jmeter版本为5.1.1
>
> 如果想要更改为其他版本，依次修改：Dockerfile、build.sh文件中的版本号5.1.1
>
> 注意： 请不要指定为低于5的版本，低于5，生产的html报告可能有问题

**注意：** 

+ 该镜像构建成功后，会带有jpgc插件，可以执行使用jpgc插件编写的脚本
+ 该镜像还对jmeter生产的html报告进行了改造，生产的报告将转换为中文