# 使用说明

## 构建jmeter主控机容器

1、下载【master-jmeter-docker】文件夹中所有文件

2、构建本地master镜像： `sh build.sh`

3、修改run-master.sh文件中 `--link`的数量和名称

> 冒号前面为slave容器名称，冒号后面为自定义别名

4、使用master容器执行分布式脚本

```sh
sh jmeter-master.sh -n -R 助攻机别名(多个时用逗号分隔) -t YouJMXfile -l YouJTL_`date +%Y%m%d_%H%M%S`.jtl -j jmeter.log -e -o report_`date +%Y%m%d_%H%M%S`
```

> 默认jmeter版本为5.1.1
>
> 如果想要更改为其他版本，依次修改：Dockerfile、build.sh文件中的版本号5.1.1
>
> 注意： 请不要指定为低于5的版本，低于5，生产的html报告可能有问题

**注意：** 

+ 该镜像构建成功后，会带有jpgc插件，可以执行使用jpgc插件编写的脚本
+ 该镜像还对jmeter生产的html报告进行了改造，生产的报告将转换为中文
+ jmeter分布式，主控和助攻机的jmeter必须一致，所以，master和slave的jmeter版本务必一致