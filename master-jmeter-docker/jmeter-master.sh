#!/bin/bash
#
# Run JMeter Docker image with options

NAME="jmeter-master"
JMETER_VERSION=${JMETER_VERSION:-"5.1.1"}
IMAGE="nmb/jmeter-master:${JMETER_VERSION}"

# Finally run
docker run --rm --name ${NAME} -i -v ${PWD}:${PWD} --link slave1:slave1 --link slave2:slave2 --link slave3:slave3 -w ${PWD} ${IMAGE} $@
