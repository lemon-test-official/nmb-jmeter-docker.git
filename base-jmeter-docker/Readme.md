# 使用说明

## 使用基础版本镜像

1、下载【base-jmeter-docker】文件夹中所有文件

2、构建本地镜像： `sh build.sh`

3、使用构建的镜像，运行jmx文件

```sh
sh jmeter.sh -n -t YouJMX_file -l JTL_`date +%Y%m%d_%H%M%S`.jtl -j jmeter.log -e -o Report_`date +%Y%m%d_%H%M%S`
```

> 默认jmeter版本为5.1.1
>
> 如果想要更改为其他版本，依次修改：Dockerfile、build.sh、run.sh文件中的版本号5.1.1

