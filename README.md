# nmb-jmeter-docker

#### 介绍
使用docker运行jmeter进行测试，可以实现在一台机器上创建出任意多个助攻机，从而大大节省性能测试时，助攻机硬件成本。



#### 软件架构
使用Dockerfile，构建docker-jmeter镜像




#### 安装教程

1.  在机器上安装docker
2.  下载代码，并上传到机器中



#### 使用说明

**注意：** 以下有四种容器，请根据实际需要使用。

##### 构建基础版jmeter容器

1、下载【base-jmeter-docker】文件夹中所有文件

2、构建本地镜像： `sh build.sh`

3、使用构建的镜像，运行jmx文件

```sh
sh jmeter.sh -n -t YouJMX_file -l JTL_`date +%Y%m%d_%H%M%S`.jtl -j jmeter.log -e -o Report_`date +%Y%m%d_%H%M%S`
```

> 默认jmeter版本为5.1.1
>
> 如果想要更改为其他版本，依次修改：Dockerfile、build.sh、run.sh文

---

##### 构建带有jpgc插件的容器

1、下载【jpgc-jmeter-docker】文件夹中所有文件

2、构建本地镜像： `sh build.sh`

3、使用构建的镜像，运行jmx文件

```sh
sh jmeter.sh -n -t YouJMX_file -l YouJTL_`date +%Y%m%d_%H%M%S`.jtl -j jmeter.log -e -o report_`date +%Y%m%d_%H%M%S`
```

> 默认jmeter版本为5.1.1
>
> 如果想要更改为其他版本，依次修改：Dockerfile、build.sh、run.sh文件中的版本号5.1.1
>
> 注意： 请不要指定为低于5的版本，低于5，生产的html报告可能有问题

---

##### 一台机器上构建多个jmeter助攻机容器

1、下载【slave-jmeter-docker】文件夹中所有文件

2、构建本地slave镜像： `sh build.sh`

3、创建slave容器

```sh
docker run -itd --name slave1 nmb/jmeter-slave:5.1.1 server

# 重复执行时，修改容器名称name值，则可创建多个slave容器
```

> 默认jmeter版本为5.1.1
>
> 如果想要更改为其他版本，依次修改：Dockerfile、build.sh文件中的版本号5.1.1
>
> 注意： 请不要指定为低于5的版本，低于5，生产的html报告可能有问题

**注意：** 

+ 该镜像构建成功后，会带有jpgc插件，可以执行使用jpgc插件编写的脚本
+ 该镜像还对jmeter生产的html报告进行了改造，生产的报告将转换为中文

---

##### 构建jmeter主控机容器

1、下载【master-jmeter-docker】文件夹中所有文件

2、构建本地master镜像： `sh build.sh`

3、修改run-master.sh文件中 `--link`的数量和名称

> 冒号前面为slave容器名称，冒号后面为自定义别名

4、使用master容器执行分布式脚本

```sh
sh jmeter-master.sh -n -R 助攻机别名(多个时用逗号分隔) -t YouJMXfile -l YouJTL_`date +%Y%m%d_%H%M%S`.jtl -j jmeter.log -e -o report_`date +%Y%m%d_%H%M%S`
```

> 默认jmeter版本为5.1.1
>
> 如果想要更改为其他版本，依次修改：Dockerfile、build.sh文件中的版本号5.1.1
>
> 注意： 请不要指定为低于5的版本，低于5，生产的html报告可能有问题

**注意：** 

+ 该镜像构建成功后，会带有jpgc插件，可以执行使用jpgc插件编写的脚本
+ 该镜像还对jmeter生产的html报告进行了改造，生产的报告将转换为中文
+ jmeter分布式，主控和助攻机的jmeter必须一致，所以，master和slave的jmeter版本务必一致



#### 参与贡献

1. Fork 本仓库

2. 新建 Feat_xxx 分支

3. 提交代码

4. 新建 Pull Request

   


#### 鸣谢

1.  如果该项目对您有帮助，请帮忙点个star
