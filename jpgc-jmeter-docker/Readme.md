# 使用说明

## 使用带有jpgc插件镜像

1、下载【jpgc-jmeter-docker】文件夹中所有文件

2、构建本地镜像： `sh build.sh`

3、使用构建的镜像，运行jmx文件

```sh
sh jmeter.sh -n -t YouJMX_file -l YouJTL_`date +%Y%m%d_%H%M%S`.jtl -j jmeter.log -e -o report_`date +%Y%m%d_%H%M%S`
```

> 默认jmeter版本为5.1.1
>
> 如果想要更改为其他版本，依次修改：Dockerfile、build.sh、run.sh文件中的版本号5.1.1
>
> 注意： 请不要指定为低于5的版本，低于5，生产的html报告可能有问题

**注意：** 

+ 该镜像构建成功后，会带有jpgc插件，可以执行使用jpgc插件编写的脚本
+ 该镜像还对jmeter生产的html报告进行了改造，生产的报告将转换为中文