#!/bin/bash
#
# Run JMeter Docker image with options

NAME="jmeter-jpgc"
JMETER_VERSION=${JMETER_VERSION:-"5.1.1-jpgc"}
IMAGE="nmb/jmeter:${JMETER_VERSION}"

# Finally run
docker run --rm --name ${NAME} -i -v ${PWD}:${PWD} -w ${PWD} ${IMAGE} $@
